#!/usr/bin/env bash
logFile='/home/mytolo/bp-app.log'
gitPATH="/home/mytolo/bp-app"


HOST="apogaming.de"
HttpPort=8090
CONTAINERHTTP=8080
EXPOSE="-p $HttpPort:$CONTAINERHTTP -p 2122:2122"
IMAGETAG=bp-app
CONTAINERNAME=bp-app

#==============================================================================


while true
do
  cd $gitPATH/code
  up=$(./bin/detect-file-change.sh $logFile)
  if [ "$up" == "1" ]
  then
    kill %1
    echo "new commit pushed"
    git checkout src/constants.js
    docker stop $CONTAINERNAME
    id=$(docker ps -aqf "name=$CONTAINERNAME")
    git pull
    sed -i.bak "s/0.0.0.0/$HOST/g" src/constants.js
    docker build -t $IMAGETAG .
    docker rm -f $id
    docker run --name $CONTAINERNAME $EXPOSE -dt $IMAGETAG
    docker ps | grep bp-app 
    docker logs -f $CONTAINERNAME &
  fi
done
