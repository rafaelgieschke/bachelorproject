// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX-ID: GPL-3.0-or-later



module.exports = function(api) {
  api.cache(true);
  const presets = ["@babel/preset-env"]
  const plugins = ["@babel/plugin-syntax-import-meta"]
  return {
    presets,
    plugins
  }
}

