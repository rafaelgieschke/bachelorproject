// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX-ID: GPL-3.0-or-later



import { logIncomingMessages, sendTestMessages, setupNetwork} from "../lib/network";
import {addLog} from "../lib/logger";

export const handleServerConnections = async (serverSocket, identifier) => {
    for await (const conn of serverSocket.readable) {
        addLog("[server]: new connection accepted")

        const writer = conn.writable.getWriter();
        for await (const buffer of conn.readable) {
            const decoded = new TextDecoder().decode(buffer);
            addLog(`[${identifier}]: ${decoded}`);

            // send reply
            await writer.write(new TextEncoder().encode(`reply: ${decoded}`));
        }

        addLog('end')
    }
}
const run = async () => {
    const {nic1, nic2} = await setupNetwork()

    // socket that accepts connections and replies
    const serverSocket = new nic1.TCPServerSocket({localPort: "8080"});
    handleServerConnections(serverSocket, 'request')

    // send messages to it
    const socket = new nic2.TCPSocket("10.0.0.1", "8080");
    logIncomingMessages(socket, 'response')
    await sendTestMessages(socket)
}

export default run