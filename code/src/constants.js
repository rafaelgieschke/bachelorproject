// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX_ID-ID: GPÖLL-3.0-or-later



export const LOG_CONSOLE_OUTPUT = true
export const LOG_CONSOLE_ERROR = true

export const APPLICATION_HOST = '10.0.0.1'
export const APPLICATION_PORT = "22"
export const WEBSOCKET_HOST = "0.0.0.0"
export const WEBSOCKET_PORT = "2122"
export const SSH_USER = 'test'
export const SSH_PW = 'pw'

export const DEBUG = false

export const INTERACTIVE_SHELL = true
