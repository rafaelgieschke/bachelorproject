import '../interactive.scss'

let runCmd = async (cmd) => {
    if (globalThis.writer.writable) {
        globalThis.writer.write(`${cmd}`)
        return `cmd ${cmd} executed`
    }
    return "Writer already closed"
}

Object.assign(globalThis, {run: runCmd})

let cmd = ""
let hist = []
let histEntry = -1
document.addEventListener('keyup', function (event) {
    if (event.defaultPrevented) {
        return;
    }
    let key = event.key
    if (key === "Enter") {
        hist[hist.length] = cmd
        runCmd(`${cmd}\r`).then(()=>{
            window.setTimeout(()=> {let log = document.getElementById('log')
                log.scrollTop = log.scrollHeight}, 1000)
        })
        if (cmd === "exit") {
            document.getElementById('cmd').style.visibility = 'hidden'
        }
        cmd = ""

    } else if (key === "Backspace") {
        cmd = cmd.substr(0, cmd.length - 1)
    } else if (key === "Control" || key === "Alt") {
        return;
    } else if (key === "ArrowRight" || key === "ArrowLeft") {
        return;
    } else if (key === "ArrowUp") {
        if (histEntry === -1) {
            histEntry = hist.length - 1
        } else {
            if (histEntry > 0) {
                histEntry -= 1
            }
        }
        cmd = hist[histEntry]
    } else if (key === "ArrowDown") {
        if (histEntry === -1) {
            return;
        }  else if (histEntry < hist.length - 1) {
            histEntry += 1
        } else {
            histEntry = -1
            cmd = ""
        }
        if (histEntry !== -1) {
            cmd = hist[histEntry]
        }
    } else if (key !== "Shift") {
        cmd += key
    }
    document.getElementById('cmd').innerHTML = `<span class="tag">[Cmd]: </span><span class="msg"> ${cmd}</span>`

});
