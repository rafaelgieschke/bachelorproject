// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX_ID-ID: GPÖLL-3.0-or-later



import {NIC} from "eaas-proxy/webnetwork";
import {addLog} from "./logger";
import {makeWSStream} from "eaas-proxy/lib/websocket-stream";

export const logIncomingMessages = async (socket, identifier) => {
    for await (const buffer of socket.readable) {
        const decoded = new TextDecoder().decode(buffer);
        addLog(`[${identifier}]: ${decoded}`)
    }
}

export const sendTestMessages = async (socket) => {
    const writer = socket.writable.getWriter();
    await writer.write(new TextEncoder().encode("test1"));
    await writer.write(new TextEncoder().encode("test2"));
    await writer.write(new TextEncoder().encode("test3"));
}

export async function setupNetwork() {
    const nic1 = await new NIC();
    nic1.addIPv4("10.0.0.1");

    const nic2 = await new NIC();
    nic2.addIPv4("10.0.0.2");

    nic1.readable.pipeThrough(nic2).pipeThrough(nic1)

    return {nic1, nic2}
}

export async function forwardConnectionData(serverSocket, webStream) {
    for await (const conn of serverSocket.readable) {
        await conn.readable.pipeThrough(webStream).pipeThrough(conn)
    }
}

export function integrateWebSocketStreamIntoNetwork(wsHost, wsPort, iface, port) {
    const websocketBasedWebStream = makeWSStream(`ws://${wsHost}:${wsPort}`)
    const sshServerSocket = new iface.TCPServerSocket({localPort: port})
    forwardConnectionData(sshServerSocket, websocketBasedWebStream).then()
}
