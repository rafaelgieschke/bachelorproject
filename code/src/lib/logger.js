// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX-ID: GPL-3.0-or-later




const oldLog = window.console.log
import {LOG_CONSOLE_OUTPUT} from "../constants";

const codeElement = document.getElementById('log')

if (LOG_CONSOLE_OUTPUT) {
    window.console.log = (msg) => {
        oldLog(msg)
        addLog(msg, 'console', false)
    }
}

export function addLog(msg, tag="info", toConsole=true) {
    if (toConsole) oldLog(`[${tag}]: ${msg}`)
    codeElement.innerHTML += `<span class="tag ${tag}">[${tag}]:</span> <span class="message ${tag}">${msg}</span>\n`
}