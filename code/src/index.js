// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX_ID-ID: GPÖLL-3.0-or-later



import 'core-js/stable'
import 'regenerator-runtime/runtime'

import './polyfills'

import './styles.scss'

import serverDemo from './demo/server-demo'
import sshDemo from "./demo/ssh-demo"

import {setupNetwork} from "./lib/network";
import {INTERACTIVE_SHELL} from "./constants";

(async () => {
    const network = await setupNetwork()
    global.picoTcpNIC = network.nic1
    await sshDemo('ls -la', network)
    if (INTERACTIVE_SHELL) {
        import('./lib/interactive.js')
    }
})()



