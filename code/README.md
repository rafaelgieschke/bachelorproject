# README

## quick start
- run `git submodule update --init --recursive`
- run `yarn`
- apply quick fixes from below
- just run `yarn start`

## quick fixes to get ssh2 working in browser


### ssh2-streams / ssh.js (Done automatically by yarn)

in `node_modules/ssh2/streams/lib/ssh.js`, search for the if block 
```js
if (!hostPubKey.verify(outstate.exchangeHash, rawsig)) {
    ...
}
```
and comment it out. It uses SHA1 which is not supported by the crypto library atm. Probably it won't be supported at any time.
[webcrypto-core](https://www.npmjs.com/package/webcrypto-core), [webcrypto-liner](https://www.npmjs.com/package/webcrypto-liner) and [webcrypto-ossl](https://www.npmjs.com/package/node-webcrypto-ossl) could be used implementing SHA1.

## todo / next steps
- improve webStreamToDuplexStream function
  - should use ex6 extend syntax, not es5 function syntax
  - closing sockets
  - memory leaks? There seem to be no leaks but this could be not 100% accurate
  - Test application with [this Post](https://www.smashingmagazine.com/2012/11/writing-fast-memory-efficient-javascript/)

## current features
  - currently the app  reimplement the Socket class of the `net` module, replacing the `net` module 
is the most generic way of solving the problem and is prefered.
There have to be further investigations whether this is enough for all other usecases and handle Streams of both 
Duplexstreams right 

## Reimplement nodejs' net-module
  replace `net` module with own implementation in `code/src/lib/net.js`
TODO(mytolo): add a webpack howto, used loaders, config scripts

### External Links
- [Dokumentation](https://github.com/nodejs/node/blob/master/doc/api/net.md)
- [Code]()
