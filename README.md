# Bachelorproject of Panajiotis Christoforidis


[Demo UI for EAAS](https://demo.emulation.cloud/admin)


# TODOS

- [ ] detailed problem description in [Bringing WHATWG-Streams and Node-Streams together](#bringing-whatwg-streams-and-nodejs-streams-together)

## Motivation

Cleary use node modules as simple as in Node. At the moment this cannot be done due to other implementations on streams by nodejs and picoTCP.

TODO(mytolo): Is this all? Ther could be sth. more...

## Stream Standards

### [Node-Streams](https://nodejs.org/api/stream.html)

#### Model

Normally nodejs streams are streams of data  operating on ``String`` and ``Unit8Array``. Nodejs streams can also operate on Objects by putting them into
Objectmode.

Basic examples of Node-Streams:
- http requests and response in http-server are streams
- process.stdout, process.stdin are streams

Kinds of Streams:
- _Writeable Streams_: Data Stream in which data can be pushed through a writer.
- _Readable Streams_: Data Stream from which data can be obtained through a reader.
- _Duplex Streams_: Data Stream data can be pushed and obtained through writer and reader.
- _Transform Streams_: Stream which is able to modify Input/Output by a transform function / callback.

#### Detailed class interfaces
<details>
<summary>TODO(mytolo): Detailed class interfaces</summary>

</details>

### [WHATWG-Streams](https://streams.spec.whatwg.org "WebHypertextApplicationTechnologyWorkingGroup Stream Standard")

#### Model

Streams consist of chunks. Chunks can have different types.

Kinds of Streams:
- _Readable Streams_: Represents data from which one can read. Code reading from _readable Streams_ is called _Consumer_ and needs to get a _reader_ from the _Readable Straem_.
- _Writeable Streams_: Represents a destination for data. Code writing into _writeable Streams_ is called _Producer_ and needs to get a _writer_ from _Writeable Stream_.
- _Transofrm Streams_: Consists of a readable Stream, called _readerable side_, and a writeable Stream, called _writeable side_. A _transformer_ specifies the function calculating the data of the _writeable side_ from the _readable side_.

Examples:
- GZIP Compressor: Uncompressed data is pushed to the _writeable side_ and the _readable side_ yields the compressed data.

##### Pipe-Chains:
Pipe Chains are basically the combination of _readable_, _writeable_ and _transform_ Streams.
```mermaid
graph LR;
RS-->TS1;
TS1-->TS2;
TS2-.->TSn;
TSn-->WS;

```

Data is guided with:  `` RS.pipeThrough(TS1).pipeThrough(TS2).....pipeThrough(TSn).pipeTo(WS) ``. (``pipeThrough`` return a readable Stream)

#### Detailed class Prototypes 

<details>
<summary>Click to expand details on Prototypes</summary>

[ReadableStream](https://streams.spec.whatwg.org/#rs-class-definition "")
```javascript
class ReadableStream {
  constructor(underlyingSource = {}, strategy = {})

  get locked()

  cancel(reason)
  getIterator({ preventCancel } = {})
  getReader({ mode } = {})
  pipeThrough({ writable, readable },
              { preventClose, preventAbort, preventCancel, signal } = {})
  pipeTo(dest, { preventClose, preventAbort, preventCancel, signal } = {})
  tee()

  [@@asyncIterator]({ preventCancel } = {})
}
```

[ReadableStreamDefaultReader](https://streams.spec.whatwg.org/#default-reader-class-definition "")

  ```javascript
  class ReadableStreamDefaultReader {
  constructor(stream)

  get closed()

  cancel(reason)
  read()
  releaseLock()
}
```

[ReadableStreamBYOBReader](https://streams.spec.whatwg.org/#byob-reader-class-definition "")

```javascript
class ReadableStreamBYOBReader {
  constructor(stream)

  get closed()

  cancel(reason)
  read(view)
  releaseLock()
}
```

[ReadableStreamBYOBRequest](https://streams.spec.whatwg.org/#rs-byob-request-class-definition "")
```javascript
class ReadableStreamBYOBRequest {
  constructor(controller, view)

  get view()

  respond(bytesWritten)
  respondWithNewView(view)
}
```

---

[WriteableStream](https://streams.spec.whatwg.org/#ws-class-definition "")
```javascript
class WritableStream {
  constructor(underlyingSink = {}, strategy = {})

  get locked()

  abort(reason)
  getWriter()
}
```

[WriteableStreamDefaultWriter](https://streams.spec.whatwg.org/#default-writer-class-definition "")
```javascript
class WritableStreamDefaultWriter {
  constructor(stream)

  get closed()
  get desiredSize()
  get ready()

  abort(reason)
  close()
  releaseLock()
  write(chunk)
}
```

[WriteableStreamDefaultController](https://streams.spec.whatwg.org/#ws-default-controller-class-definition "")
```javascript
class WritableStreamDefaultController {
  constructor() // always throws

  error(e)
}
```

[TransformStream](https://streams.spec.whatwg.org/#ts-class-definition "")
```javascript
class TransformStream {
  constructor(transformer = {}, writableStrategy = {}, readableStrategy = {})

  get readable()
  get writable()
}
```

[TransformStreamDefaultController](https://streams.spec.whatwg.org/#ts-default-controller-class-definition "")
``` javascript
class TransformStreamDefaultController {
  constructor() // always throws

  get desiredSize()

  enqueue(chunk)
  error(reason)
  terminate()
}
```
---
[ByteLengthQueuingStrategy](https://streams.spec.whatwg.org/#blqs-class-definition "Byte Length Queing Strategy for the internal queue")
_wait until the accumulated byteLength properties of the incoming chunks reaches a specified high-water mark_
```javascript
class ByteLengthQueuingStrategy {
  constructor({ highWaterMark })
  size(chunk)
}
```

[CountQueuingStrategy](https://streams.spec.whatwg.org/#cqs-class-definition "")
_count the number of chunks that have been accumulated_

```javascript
class CountQueuingStrategy {
  constructor({ highWaterMark })
  size(chunk)
}
```
</details>

## [Browserify](https://github.com/browserify/browserify "Browser-side require")
_Browserify_ is a tool that enables the use of Node Commonjs Modules for the browser. Basically the tool recursively searches for all require calls
doing a static analysis of the Abstract Syntax Tree of the code.

The content of all files will be contatenated and the minimal required modules are calculated. The file self-contains all of its dependencies. Therefore some of the nodejs core
modules are reimplemented for the browser by browserify.

For _EAAS_ _Broserify_ is used to enable a Proxy Connection to the emulated images through nodejs.

A alternative would be to use [webpack](https://webpack.js.org "") as bundler for the browser.
<details>
<summary>Information about webpack</summary>
Webpack's default target is web. It can bundle almost every asset.

> _At its core, webpack is a static module bundler for modern JavaScript applications. When webpack processes your application, it internally builds a dependency graph which maps every module your project needs and generates one or more bundles._
>
> <cite>[Webpack Concepts](https://webpack.js.org/concepts/)</cite>

Webpack only support Browsers that are _ES5-compliant_. 

Webpack needs `Promise` for `import()` and `require.ensure()`. If you want to support older browsers, you will need to load a [polyfill](https://webpack.js.org/guides/shimming/) before using these expressions.
</details>

A sample builduing a proxy is provided by [eaas-proxy.js](examples/eaas-proxy.js)


### [net-browserify (TCP)](https://github.com/emersion/net-browserify "Browserified TCP API") and [dgram-browserify](https://github.com/alexstrat/dgram-browserify "Browserified UDP nodejs API")

Sources:
- [net TCP-API nodejs](https://nodejs.org/api/net.html)
- [User datagram protocol (UDP) API Nodejs](https://nodejs.org/api/net.html)


## Bringing WHATWG Streams and Nodejs Streams together

### Purpose of this Project

The both stream models discused about previously are different. In order to use libraries operating on different stream standards a Mediator Stream would be useful.
This mediator would e.g. take nodejs streams and transform them into WHATWG-Streams.



# Links


## Local Filesystem in Browser
- [Chromes Native File System API](https://developers.google.com/web/updates/2019/08/native-file-system)
- For Firefox there is no Filesystem API
