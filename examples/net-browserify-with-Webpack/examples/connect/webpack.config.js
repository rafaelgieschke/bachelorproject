const apth = require('path')

module.exports = {
  entry: './client.js',
  target: 'web',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname)
  }
}
