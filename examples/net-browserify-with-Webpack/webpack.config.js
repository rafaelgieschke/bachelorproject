const path = require('path')

const client = {
  entry: './examples/connect/client.js',
  target: 'web',
  mode: 'production',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './examples/connect/')
  }
}

const server = {
  entry: './examples/connect/server.js',
  target: 'web',
  mode: 'production',
  output: {
    filename: 'bundleServer.js',
    path: path.resolve(__dirname, './examples/connect/')
  }
}

module.exports = [client]
