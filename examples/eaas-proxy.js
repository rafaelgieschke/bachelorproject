<script type="module">
import { NIC } from "https://emulation-as-a-service.gitlab.io/eaas-proxy/webnetwork.js";

/*
There are at least three different examples of APIs exposing TCP/UDP
client/server sockets to the Web platform:

- [TCP and UDP Socket API](https://www.w3.org/TR/tcp-udp-sockets/)
- [Webextensions TCP API proposal](https://github.com/mozilla/libdweb/wiki/API-Proposal-TCP)
- [WebSocketStream Explained](https://github.com/ricea/websocketstream-explainer)
  (only exposes client sockets)

All of them are based on [WHATWG's Streams Standard](https://streams.spec.whatwg.org/).

webnetwork.js is (loosely) based on the _TCP and UDP Socket API_.
*/

// (Incomplete) polyfill for <https://streams.spec.whatwg.org/#rs-get-iterator>
ReadableStream.prototype[Symbol.asyncIterator] = async function* () {
    const reader = this.getReader();
    reader.next = reader.read;
    globalThis.a = reader;
  // trying if data is there if not, cancel the reader
    try {
        for await (const value of reader) yield value;
    } finally {
        reader.cancel();
    }
};

(async () => {
    // set up network interface cards
    const nic1 = await new NIC();
    nic1.addIPv4("10.0.0.1");

    const nic2 = await new NIC();
    nic2.addIPv4("10.0.0.2");

    nic1.readable.pipeThrough(nic2).pipeThrough(nic1);

    const serverSocket = new nic1.TCPServerSocket(
        { localPort: "8080" });
    const socket = new nic2.TCPSocket("10.0.0.1", "8080");



    (async () => {
        for await (const conn of serverSocket.readable) {
            console.log("New connection");
            for await (const buffer of conn.readable) {
                console.log(buffer, new TextDecoder().decode(buffer));
            }
        }
    })();

    const writer = socket.writable.getWriter();
    writer.write(new TextEncoder().encode("test"));
    writer.write(new TextEncoder().encode("test2"));

    Object.assign(globalThis, {
        nic1, nic2, serverSocket, socket
    });
})();
</script>
