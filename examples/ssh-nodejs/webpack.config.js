const path = require('path')

const ssh_client = {
  entry: './ssh-interactive.js',
  mode: 'development',
  target: 'web',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname)
  }
}

module.exports = [ssh_client]
